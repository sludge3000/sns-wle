#!/usr/bin/env python3
#
# Stormshield Network Security Web Log Enumerator
# Version 0.02 - BETA
# created by Luke "sludge3000" Savage
#

# Import modules
import argparse
import os
import re

from pathlib import Path


templist1 = []
templist2 = []


def tlc():
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []


# Simple iterator
def iter(filter):
    global templist1
    global templist2
    for line in templist1:
        if filter in line:
            templist2.append(line)


def extract():
    p = Path(os.getcwd())
    files = list(p.glob('l_web*'))
    for file in files:
        iFile = open(file)
        for line in iFile:
            templist2.append(line)
        iFile.close()


def filter_user(username):
    username = 'user="' + username + '"'
    tlc()
    iter(username)


# Find better name for function
def collect_destnames():
    tlc()
    for line in templist1:
        regex = re.search(r'dstname=(\S+)', line)
        if regex:
            templist2.append(regex.group(1))


def sort_n_print():
    sorted = set(templist2)
    oFile = open('test.txt', 'w+')
    for line in sorted:
        oFile.write(line + '\n')
    oFile.close()


def snswle():
    args = parser.parse_args()
    username = args.user
    extract()

    if username is not None:
        filter_user(username)

    collect_destnames()
    sort_n_print()


def main():
    snswle()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
parser.add_argument("-u", "--user", default=None,
                    help="Specify the username to filter.")


main()
